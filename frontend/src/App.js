import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import ProductList from "./containers/ProductList/ProductList";
import ProductInfo from "./containers/ProductInfo";
import NewProduct from "./containers/NewProduct/NewProduct";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Layout>
                <Switch>
                    <Route path="/" exact component={ProductList}/>
                    <Route path="/categories/:id" exact component={ProductList}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/products/:id" exact component={ProductInfo}/>
                    <Route path="/create_product" exact component={NewProduct}/>
                </Switch>
            </Layout>
        </Fragment>
    );
  }
}


export default App;

