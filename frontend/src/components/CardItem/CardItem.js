import React, {Fragment} from 'react';
import {Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import PropTypes from 'prop-types';



const CardItem = props => {
    return (
        <Fragment>
            <Card onClick={props.click} style={{cursor: 'pointer'}}>
                <CardImg top width="100%" src={props.imageSrc} alt={props.imageAlt}/>
                    <CardBody>
                        <CardTitle>{props.title}</CardTitle>
                        <CardSubtitle>{props.subtitle}</CardSubtitle>
                        <CardText>{props.text}</CardText>
                    </CardBody>
            </Card>
        </Fragment>
    );
};

CardItem.propTypes = {
    imageSrc: PropTypes.string,
    imageAlt: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    subtitle: PropTypes.string,
    click: PropTypes.func
};

export default CardItem;