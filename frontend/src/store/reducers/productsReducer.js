import {GET_PRODUCTS_FAILURE, GET_PRODUCTS_SUCCESS} from "../actions/productsActions";

const initialState = {
    products: [],
    error: null
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS_SUCCESS:
            return {...state, products: action.data};
        case GET_PRODUCTS_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default productsReducer;
