import {GET_CATEGORIES_FAILURE, GET_CATEGORIES_SUCCESS} from "../actions/categoriesActions";


const initialState = {
    categories: [],
    error: null
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES_SUCCESS:
            return {...state, categories: action.data};
        case GET_CATEGORIES_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default categoriesReducer;
