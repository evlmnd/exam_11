import {GET_PRODUCT_INFO_FAILURE, GET_PRODUCT_INFO_SUCCESS} from "../actions/productInfoActions";

const initialState = {
    product: {},
    error: null
};

const productInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCT_INFO_SUCCESS:
            return {...state, product: action.data};
        case GET_PRODUCT_INFO_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default productInfoReducer;