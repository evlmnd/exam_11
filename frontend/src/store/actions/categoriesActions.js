import axios from '../../axios-api';

export const GET_CATEGORIES_SUCCESS = 'GET_CATEGORIES_SUCCESS';
export const GET_CATEGORIES_FAILURE = 'GET_CATEGORIES_FAILURE';

const getCategoriesSuccess = data => ({type: GET_CATEGORIES_SUCCESS, data});
const getCategoriesFailure = error => ({type: GET_CATEGORIES_FAILURE, error});

export const getCategories = () => {
    return dispatch => {
        return axios.get('/categories').then(response => {
                dispatch(getCategoriesSuccess(response.data));
            },
            error => {
                dispatch(getCategoriesFailure(error));
            }
        )
    }
};