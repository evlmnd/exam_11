import axios from '../../axios-api';

export const GET_PRODUCT_INFO_SUCCESS = 'GET_PRODUCT_INFO_SUCCESS';
export const GET_PRODUCT_INFO_FAILURE = 'GET_PRODUCT_INFO_FAILURE';

const getProductInfoSuccess = data => ({type: GET_PRODUCT_INFO_SUCCESS, data});
const getProductInfoFailure = error => ({type: GET_PRODUCT_INFO_FAILURE, error});

export const getProductInfo = route => {
    return dispatch => {
        return axios.get(route).then(response => {
                dispatch(getProductInfoSuccess(response.data));
            },
            error => {
                dispatch(getProductInfoFailure(error));
            }
        )
    }
};