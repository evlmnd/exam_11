import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';

const getProductsSuccess = data => ({type: GET_PRODUCTS_SUCCESS, data});
const getProductsFailure = error => ({type: GET_PRODUCTS_FAILURE, error});

export const getProducts = () => {
    return dispatch => {
        return axios.get('/products').then(response => {
                dispatch(getProductsSuccess(response.data));
            },
            error => {
                dispatch(getProductsFailure(error));
            }
        )
    }
};

export const createProduct = data => {
    return (dispatch, getState) => {
        const token = getState().users.user.user.token;
        const headers = {'Token': token};

        return axios.post('/products', data, {headers}).then(response => {
                dispatch(getProducts());
                dispatch(push('/'));
            },
            error => {
                console.log(error); //
            }
        )
    }
};

export const getCategoryProducts = id => {
    return dispatch => {
        return axios.get('/products/categories/' + id).then(response => {
                dispatch(getProductsSuccess(response.data));
            },
            error => {
                dispatch(getProductsFailure(error));
            }
        )
    }
};

export const deleteProduct = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.user.token;
        const headers = {'Token': token};

        return axios.delete('/products/' + id, {headers}).then(response => {
                dispatch(getProducts());
                dispatch(push('/'));
            },
            error => {
                console.log(error); //
            }
        )
    }
};