import React, {Component, Fragment} from 'react';
import {Button, Jumbotron, Media, NavLink} from "reactstrap";

import {getProductInfo} from "../store/actions/productInfoActions";
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from "react-router-dom";
import config from "../config";
import {deleteProduct} from "../store/actions/productsActions";

class ProductInfo extends Component {

    onDeleteProduct = (event, id) => {
        event.preventDefault();
        this.props.deleteProduct(id);
    };

    componentDidMount() {
        this.props.getProductInfo(this.props.location.pathname);
    };

    render() {
        const item = this.props.product;

        return (
            <Fragment>
                {
                    (this.props.product.seller && this.props.product.category) ?
                        <Jumbotron>
                            <NavLink tag={RouterNavLink} to={"/categories/" + item.category._id}
                                     exact>Category: {this.props.product.category.title}</NavLink>
                            <h1 className="display-3">{item.title}</h1>
                            <img src={config.apiUrl + '/uploads/' + item.image} alt={item.name}/>
                            <p className="lead">{item.description}</p>
                            <hr className="my-2"/>
                            <p>${item.price}</p>

                            {this.props.user ?

                                ((this.props.user.user._id === item.seller._id) ?
                                    <Button color="danger"
                                            onClick={(event) => this.onDeleteProduct(event, item._id)}>Delete</Button>
                                    :
                                    <p className="lead">From {item.seller.displayName} ({item.seller.phoneNumber})</p>)

                                :
                                <p className="lead">From {item.seller.displayName} ({item.seller.phoneNumber})</p>
                            }
                            {/*некрасиво написано конечно, да(( */}

                        </Jumbotron>
                        : null
                }
            </Fragment>
        );

    }
}

const mapStateToProps = state => ({
    product: state.product.product,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getProductInfo: route => dispatch(getProductInfo(route)),
    deleteProduct: id => dispatch(deleteProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductInfo);