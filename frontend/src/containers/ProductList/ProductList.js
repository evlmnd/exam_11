import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {getCategoryProducts, getProducts} from "../../store/actions/productsActions";
import {CardDeck, Col} from "reactstrap";
import CardItem from "../../components/CardItem/CardItem";
import config from "../../config";

class ProductList extends Component {

    componentDidMount() {
        if (!this.props.match.params.id) {
            this.props.getProducts();
        } else {
            this.props.getCategoryProducts(this.props.match.params.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.pathname !== this.props.location.pathname) {
            if (nextProps.match.params.id) {
                this.props.getCategoryProducts(nextProps.match.params.id);
            } else {
                this.props.getProducts();
            }
        }
    }

    getProductInfo = id => {
        this.props.history.push('/products/' + id);
    };

    render() {

        const products = this.props.products.map(item => {
            return <Col sm={3} key={item._id}>
                <CardItem
                    title={item.title}
                    subtitle={'$' + item.price}
                    click={() => this.getProductInfo(item._id)}
                    imageSrc={config.apiUrl + '/uploads/' + item.image}
                />
            </Col>
        });

        return (
            <Fragment>
                <CardDeck>
                    {products}
                </CardDeck>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.products.products
});

const mapDispatchToProps = dispatch => ({
    getProducts: () => dispatch(getProducts()),
    getCategoryProducts: id => dispatch(getCategoryProducts(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);