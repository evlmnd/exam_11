import React, {Component, Fragment} from 'react';
import ProductForm from "../../components/ProductForm/ProductForm";
import {connect} from "react-redux";
import {getCategories} from "../../store/actions/categoriesActions";
import {createProduct} from "../../store/actions/productsActions";

class NewProduct extends Component {
    componentDidMount() {
        this.props.getCategories();
    }

    createProduct = productData => {
        this.props.onProductCreated(productData)
            .then(() => {
                this.props.history.push('/');
            });
    };

    render() {
        return (
            <Fragment>
                <h2>New product</h2>
                <ProductForm
                    onSubmit={this.createProduct}
                    categories={this.props.categories}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    onProductCreated: productData => dispatch(createProduct(productData)),
    getCategories: () => dispatch(getCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);
