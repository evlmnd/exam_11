import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";

import {logoutUser} from "../../store/actions/usersActions";
import Toolbar from "../../components/UI/Toolbar/Toolbar";
import {getCategories} from "../../store/actions/categoriesActions";

class Layout extends Component {

    componentDidMount() {
        this.props.getCategories();
    }

    render() {

        return (
            <Fragment>
                <header>
                    <Toolbar
                        user={this.props.user}
                        logout={this.props.logoutUser}
                        categories={this.props.categories}
                    />
                </header>
                <Container style={{marginTop: '20px'}}>
                    {this.props.children}
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser()),
    getCategories: () => dispatch(getCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);