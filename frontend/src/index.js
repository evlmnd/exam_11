import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware, ConnectedRouter} from 'connected-react-router';
import * as serviceWorker from './serviceWorker';

import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import usersReducer from "./store/reducers/usersReducer";
import categoriesReducer from "./store/reducers/categoriesReducer";
import productsReducer from "./store/reducers/productsReducer";
import productInfoReducer from "./store/reducers/productInfoReducer";
import {readState, saveState} from "./store/localStorage";

const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    categories: categoriesReducer,
    products: productsReducer,
    product: productInfoReducer,
    users: usersReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = readState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({users: {user: store.getState().users.user}});
});

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();



