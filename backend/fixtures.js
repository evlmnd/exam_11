const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create({
        username: 'user1',
        password: '1',
        displayName: 'UserOne',
        phoneNumber: '+11111111'
    }, {
        username: 'user2',
        password: '2',
        displayName: 'UserTwo',
        phoneNumber: '+2222222'
    });

    const categories = await Category.create(
        {title: 'Household'},
        {title: 'Cars'},
        {title: 'Beauty'},
        {title: 'Electronics'}
    );

    await Product.create(
        {
            title: 'Диван',
            price: 500,
            description: 'лучший на свете диван',
            category: categories[0]._id,
            image: 'fixtures/divan.jpg',
            seller: users[0]._id
        },
        {
            title: 'Toyota AE86',
            description: 'Super classic drift car',
            price: 2000,
            category: categories[1]._id,
            image: 'fixtures/ae86.jpg',
            seller: users[1]._id
        },
        {
            title: 'Nissan Skyline R32 GTR',
            description: 'Pushka gonka',
            price: 3000,
            category: categories[1]._id,
            image: 'fixtures/r32.jpg',
            seller: users[0]._id
        },
        {
            title: 'Hybride technology',
            description: 'Go out and make some sport',
            price: 30,
            category: categories[1]._id,
            image: 'fixtures/hybride.jpg',
            seller: users[0]._id
        },
        {
            title: 'Nokia integrated system',
            description: 'we are connecting people',
            price: 100,
            category: categories[3]._id,
            image: 'fixtures/nokia.jpg',
            seller: users[1]._id
        },
        {
            title: 'Morning panic',
            description: 'Helps you to be in time for work',
            price: 100,
            category: categories[3]._id,
            image: 'fixtures/morning.jpg',
            seller: users[1]._id
        },
        {
            title: 'Sprite & Vetements collab',
            description: 'Samiy modniy na rayone',
            price: 200,
            category: categories[2]._id,
            image: 'fixtures/sprite.jpg',
            seller: users[0]._id
        },
        {
            title: 'Nap pigamas',
            description: 'Always ready for a nap',
            price: 50,
            category: categories[2]._id,
            image: 'fixtures/pigamas.jpg',
            seller: users[0]._id
        },
        {
            title: 'Friday hat',
            description: 'It\'s friday',
            price: 100,
            category: categories[2]._id,
            image: 'fixtures/hat.jpg',
            seller: users[0]._id
        },
        {
            title: 'Лавандовый раф',
            description: 'Pushka gonka',
            price: 1000,
            category: categories[1]._id,
            image: 'fixtures/raf.jpg',
            seller: users[1]._id
        },
        {
            title: 'Sweater with dears',
            description: 'For real fashionistas',
            price: 300,
            category: categories[2]._id,
            image: 'fixtures/sweater.jpg',
            seller: users[0]._id
        },
        {
            title: 'Future 3000',
            description: 'Дощечка 3000',
            price: 300,
            category: categories[0]._id,
            image: 'fixtures/3000.jpg',
            seller: users[1]._id
        },
        {
            title: 'Upgraded mirror',
            description: 'Take best for your car',
            price: 400,
            category: categories[1]._id,
            image: 'fixtures/mirror.jpg',
            seller: users[0]._id
        },
        {
            title: 'Thirsty?',
            description: 'Get some tea',
            price: 40,
            category: categories[0]._id,
            image: 'fixtures/tea.jpg',
            seller: users[0]._id
        },
        {
            title: 'The banana call',
            description: 'My mom says i am normal',
            price: 200,
            category: categories[3]._id,
            image: 'fixtures/banana.jpg',
            seller: users[1]._id
        },
        {
            title: 'iSpider',
            description: 'Last years flagman',
            price: 1000,
            category: categories[3]._id,
            image: 'fixtures/ispider.jpg',
            seller: users[0]._id
        },
        {
            title: 'I dont hear you',
            description: 'Banana vibes',
            price: 3000,
            category: categories[3]._id,
            image: 'fixtures/bananavibes.jpg',
            seller: users[0]._id
        }

    );


    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
